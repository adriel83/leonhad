package com.example.demo.config;

import com.example.demo.entity.*;
import com.example.demo.repository.*;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent>{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProdutoRepository produtoRepository;
    @Autowired
    private CarrinhoRepository carrinhoRepository;
    @Autowired
    private CategoriaRepository categoriaRepository;
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        List<Categoria> categorias = categoriaRepository.findAll();
        if (categorias.isEmpty()){
            categorias.add(new Categoria("categoria"));
            categorias.add(new Categoria("categoria 2"));
            categoriaRepository.saveAll(categorias);
        }
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            users.add(new User("usuario", "123"));
            users.add(new User("シャナ", "132"));
            userRepository.saveAll(users);
        }
        List<Produto> produtos = produtoRepository.findAll();
        if (produtos.isEmpty()) {
            produtos.add(new Produto("produto", categoriaRepository.findAll(), 150));
            produtos.add(new Produto("produto2", categoriaRepository.findAll(), 230));
            produtos.add(new Produto("produto3", categoriaRepository.findAll(), 120));
            produtos.add(new Produto("produto4", categoriaRepository.findAll(), 550));
            produtos.add(new Produto("produto5", categoriaRepository.findAll(), 80));
            produtoRepository.saveAll(produtos);
        }
        List<Carrinho> carrinhos = carrinhoRepository.findAll();
        if (carrinhos.isEmpty()) {
            carrinhos.add(new Carrinho(produtos.subList(1, 2), users.get(0)));
            carrinhos.add(new Carrinho(produtos.subList(0, 3), users.get(1)));
            carrinhos.add(new Carrinho(produtos, users.get(1)));
            carrinhoRepository.saveAll(carrinhos);
        }
        List<Pagamento> pagamentos = pagamentoRepository.findAll();
        if (pagamentos.isEmpty()) {
            pagamentos.add(new Pagamento(users.get(0), carrinhos.get(0)));
            pagamentos.add(new Pagamento(users.get(1), carrinhos.get(1)));
            pagamentos.add(new Pagamento(users.get(1), carrinhos.get(2)));
            pagamentoRepository.saveAll(pagamentos);
        }
    }
}
