package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Carrinho {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToMany
    @JoinColumn(unique = false)
    private List<Produto> produtos;
    @ManyToOne
    private User user;
    private float valor;

    @Override
    public String toString(){
        return produtos.toString();
    }

    public Carrinho(){

    }

    public Carrinho(List<Produto> produtos, User user){
        this();
        this.user = user;
        this.produtos = produtos;
        for (Produto produto : produtos) {
            this.valor = this.valor + produto.getValor();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
}
