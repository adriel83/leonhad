package com.example.demo.controller;

import com.example.demo.entity.Categoria;
import com.example.demo.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
    @Autowired
    CategoriaRepository categoriaRepository;

    @GetMapping
    public List<Categoria> get(){
        return categoriaRepository.findAll();
    }
    @GetMapping(path = "{id}")
    public Categoria list(@PathVariable Long id){
        return categoriaRepository.getOne(id);
    }
    @PostMapping
    public Categoria create(@RequestBody Categoria categoria){
        categoria.setId(0L);
        return categoriaRepository.save(categoria);
    }
}
