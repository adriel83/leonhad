package com.example.demo.controller;

import com.example.demo.entity.Carrinho;
import com.example.demo.entity.Produto;
import com.example.demo.repository.CarrinhoRepository;
import com.example.demo.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/carrinho")
public class CarrinhoController {
    @Autowired
    private CarrinhoRepository carrinhoRepository ;
    @Autowired
    private ProdutoRepository produtoRepository ;

    @GetMapping
    public List<Carrinho> list(){ return carrinhoRepository.findAll(); }

    @GetMapping(path ="{id}")
    public Carrinho get(@PathVariable Long id){ return carrinhoRepository.getOne(id); }

    @PostMapping
    public Carrinho create(@RequestBody Carrinho carrinho){
        carrinho.setId(0L);
        List<Produto> produtos = carrinho.getProdutos();
        for (Produto produto : produtos) {
            carrinho.setValor(produtoRepository.getOne(produto.getId()).getValor() + carrinho.getValor());
        }
        return carrinhoRepository.save(carrinho);
    }
}
