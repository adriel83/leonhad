package com.example.demo.controller;

import com.example.demo.entity.Produto;
import com.example.demo.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoRepository produtoRepository;

    @GetMapping
    public List<Produto> list(){ return produtoRepository.findAll(); }

    @GetMapping(path ="{id}")
    public Produto get(@PathVariable Long id){ return produtoRepository.getOne(id); }

    @PostMapping
    public Produto create(@RequestBody Produto produto){
        produto.setId(0L);
        return produtoRepository.save(produto);
    }
}
